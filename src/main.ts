import { app, BrowserWindow, ipcMain } from "electron";
import * as path from "path";
import {exec} from "child_process";

let mcVersion: string;
function findVersion() {
	let idx = process.argv.findIndex( (e) => {
		return e.startsWith("--mc-version=");
	});
	let arg = process.argv[idx];
	mcVersion = arg.split('=')[1];
}
findVersion();


function download(url: string, downloadDir:string): void {
	const fileName = url.split(path.sep).at(-1);
	const filePath = path.join(downloadDir, fileName);
	
	const cmd = `wget "${url}" -O "${filePath}"`;
	console.log(cmd);
	exec(cmd, (error, stdout, stderr) => {
		if (error) {
			console.error(`error: ${error.message}`);
			return;
		}
		if (stderr) {
			console.error(`stderr: ${stderr}`);
			return;
		}
	});
}

function createWindow() {
	const mainWindow = new BrowserWindow({
    	height: 600,
    	width: 800,
		minWidth: 600,
		minHeight: 450,
    	webPreferences: {
			preload: path.join(__dirname, "preload.js"),
    	},
		frame: false,
	});

    ipcMain.handle('download-url', async (event, url) => {
		const dlPath = path.join('/home/rocketdave/.local/share/multimc/instances/Mod downloader test/.minecraft', '/mods');
    	download(url,dlPath);
		return;
    });

	ipcMain.handle('getMinecraftVersion', async (event) => {
		return mcVersion;
	});

	mainWindow.loadFile(path.join(__dirname, "../index.html"));
}


app.on("ready", () => {
	createWindow();

	app.on("activate", function () {
		// On macOS it's common to re-create a window in the app when the
		// dock icon is clicked and there are no other windows open.
		if (BrowserWindow.getAllWindows().length === 0) createWindow();
	});
});

app.on("window-all-closed", () => {
	if (process.platform !== "darwin") {
		app.quit();
	}
});

