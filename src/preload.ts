import { contextBridge, ipcRenderer } from 'electron'

contextBridge.exposeInMainWorld('electronAPI', {
    downloadUrl: (url: string) => ipcRenderer.invoke('download-url', url),
	getMinecraftVersion: () => ipcRenderer.invoke('getMinecraftVersion')
})


