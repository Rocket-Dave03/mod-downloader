let modListElement: Element;
let currenlyLoadingMods: boolean = false;
let shouldLoadMods: boolean = false;
let loadedModCount = 0

function createModEntry(modImgUrl: string, modID: string, modTitle: string): Element{
	const modEntryNode 		= document.createElement('div');
	const modImageNode 		= document.createElement('img');
	const modImageDivNode	= document.createElement('div');
	const modTitleNode 		= document.createElement('span');
	const downloadButtonNode= document.createElement('div');
	const downloadTextNode	= document.createElement('span'); 
	modEntryNode.setAttribute('class', 'mod-entry');
	modImageNode.setAttribute('src', modImgUrl);
	modImageNode.setAttribute('class', 'mod-icon');
	modImageDivNode.setAttribute('class', 'mod-icon-container');
	modTitleNode.setAttribute('class', 'mod-title');
	downloadTextNode.setAttribute('class', 'download-text');
	downloadTextNode.setAttribute('data-id', modID);
	downloadButtonNode.setAttribute('class', 'download-button');
	downloadButtonNode.setAttribute('data-id', modID);
	modTitleNode.innerHTML = modTitle;
	downloadTextNode.innerHTML = 'Download';
	downloadButtonNode.addEventListener('click', downloadButtonEventHandler);
	downloadButtonNode.appendChild(downloadTextNode);
	modImageDivNode.appendChild(modImageNode);
	modEntryNode.appendChild(modTitleNode);
	modEntryNode.appendChild(modImageDivNode);
	modEntryNode.appendChild(downloadButtonNode);

	return modEntryNode;
}


async function loadMods(): Promise<void> {
	if(!currenlyLoadingMods) {
		console.log("Loading Mods!");
		currenlyLoadingMods = true;
		const mcVersion: string = await window.electronAPI.getMinecraftVersion();
		const searchUrl = `https://api.modrinth.com/v2/search?limit=25&offset=${loadedModCount}&facets=[["versions:${mcVersion}"]]`;
		const res = await fetch(searchUrl);
		if (res.ok) {
			const json = await res.json();
			//console.log(json);
			const mods = json.hits;
			for (let i = 0; i < mods.length; i++) {
				const mod = mods[i];
				//console.log(mod);
				const modEntry = createModEntry(mod.icon_url, mod.project_id, mod['title']);
				modListElement.appendChild(modEntry);
				loadedModCount++;
			}
		} else {
			console.error('HTTP-error: ' + res.status);
			setTimeout(() => {
				currenlyLoadingMods = false;
				shouldLoadMods = false;
			}, 5000);
			return;
		}
	}
	setTimeout(() => {
		currenlyLoadingMods = false;
		shouldLoadMods = false;
	}, 100);
}

async function downloadModById(modID: string): Promise<void> {
	const requestUrl = `https://api.modrinth.com/v2/project/${modID}/version`;
	const res = await fetch(requestUrl);
	if( res.ok) {
		const mcVersion: string = await window.electronAPI.getMinecraftVersion();
		console.log(mcVersion);
		const json = await res.json();
		let downloadUrls : Array<string> = [];

		// Loop through each available version of the mod
		for(let i = 0; i < json.length;i++){
			const version = json[i];
			// Check if this version if for the currently selectected Minecraft version
			if(version['game_versions'].includes(mcVersion)) {
				console.log(version['id']);	
				const files = version['files'];
				// Loop through all the files for each version and find the correct one
				if (files.length > 1) {
					for (let i = 0; i < files.length; i++) {
						if (files[i]['primary']) {
							downloadUrls.push(files[i]['url']);
						}
					}
				} else {
					downloadUrls.push(files[0]['url']);
				}
			}
		}
		window.electronAPI.downloadUrl(downloadUrls[0]);
	} else {
		console.error('HTTP-Error: ' + res.status);
		return;
	}
}

function downloadButtonEventHandler(event: Event) {
	const elem = event.target as Element;
	const modID = elem.getAttribute('data-id');
	downloadModById(modID);
}
function scrollEventHandler(event: Event): void {
	const elem = event.target as Element;
	
	const distRemaining = (elem.scrollHeight - elem.clientHeight) - elem.scrollTop;
	//console.log(distRemaining);
	if (distRemaining <= 400) {
		shouldLoadMods = true;
	}
}

function load() {
	currenlyLoadingMods = false;
	modListElement = document.getElementById('mod-list');
	modListElement.addEventListener('scroll', scrollEventHandler);
	loadMods();

	setInterval(() => {
		if(shouldLoadMods && !currenlyLoadingMods) {
			loadMods();
		}
	}, 500);
	//for(let i = 0; i < 10; i++){
	//	modListElement.appendChild(
	//		createModEntry(
	//			'https://cdn.modrinth.com/data/49ZofO4f/b2030ea8c59bccd592dfff537987c8d4a4d43dec.png', 
	//			'https://cdn.modrinth.com/data/49ZofO4f/versions/5.3.70/EnderIO-1.12.2-5.3.70.jar',
	//			'Ender IO'
	//		)
	//	); 
	// }
}
