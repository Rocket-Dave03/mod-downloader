export interface IElectronAPI {
	downloadUrl: (url: string) => Promise<any>,
	getMinecraftVersion: () => Promise<string>
}

declare global {
	interface Window {
		electronAPI: IElectronAPI
	}
}
